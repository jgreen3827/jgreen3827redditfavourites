import React, { useState } from 'react';
import './App.css';
import NavBar from './components/navBar';
import { Route, Routes } from 'react-router-dom';
import Home from './routes/home';
import Search from './routes/search';
import { UserContext } from './context/UserContext';
import Favourites from './routes/favourites';

function App() {
  const [user, setUser] = useState("");

  return (
    <div className="App">
      <UserContext.Provider value={{user, setUser}} >
        <header className="App-header">
          <NavBar />
        </header>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="search" element={<Search />} />
          <Route path="favourites" element={<Favourites />} />
        </Routes>
      </UserContext.Provider>
    </div>
    
  );
}

export default App;
