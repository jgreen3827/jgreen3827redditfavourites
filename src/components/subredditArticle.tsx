import { Grid, Paper, Typography, Link, Button } from "@mui/material";
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import React, {useState, useContext, useEffect} from 'react';
import { userModel } from "../models/storageModel";
import { UserContext } from "../context/UserContext";

export type subredditArticleProps = {
  title: string,
  score: number,
  link: string,
  id: string
}

export default function SubredditArticle({title, score, link, id}: subredditArticleProps) {
  const [favourited, setFavourited] = useState(false);
  const currentUser = useContext(UserContext);

  useEffect(() => {
    setFavourited(false);
    const inUsers = localStorage.getItem("users");
    if(inUsers !== null){
      const parsedUsers:userModel[] = JSON.parse(inUsers);
      for(let i = 0; i < parsedUsers.length; i++) {
        if(parsedUsers[i].userName === currentUser.user){
         for(let j = 0; j < parsedUsers[i].favourites.length; j++){
           if(parsedUsers[i].favourites[j].id === id){
            setFavourited(true);
            return;
           }
         }
        }
      }
    }
  }, [currentUser.user, id]);

  const selectFavourite = () => {
    setFavourited(true);
    const inUsers = localStorage.getItem("users");
    if(inUsers !== null){
      const parsedUsers:userModel[] = JSON.parse(inUsers);
      for(let i = 0; i < parsedUsers.length; i++) {
        if(parsedUsers[i].userName === currentUser.user){
          parsedUsers[i].favourites.push({
            id: id
          })
          break;
        }
      }
      localStorage.setItem("users", JSON.stringify(parsedUsers));
    }
  }

  const deselectFavourite = () => {
    setFavourited(false);

    const inUsers = localStorage.getItem("users");
    if(inUsers !== null){
      const parsedUsers:userModel[] = JSON.parse(inUsers);
      for(let i = 0; i < parsedUsers.length; i++) {
        if(parsedUsers[i].userName === currentUser.user){
         for(let j = 0; j < parsedUsers[i].favourites.length; j++){
           if(parsedUsers[i].favourites[j].id === id){
            parsedUsers[i].favourites.splice(j, 1);
           }
         }
        }
      }
      localStorage.setItem("users", JSON.stringify(parsedUsers));
    }
  }
  return (
      <Paper style={{marginTop: '20px', marginBottom: '20px'}}>
        <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '20px', paddingBottom: '20px'}}>
          <Grid item xs={11}>
            <Link href={link} target="_blank" style={{color: '#000000', textDecoration: 'none'}}>
              <Typography variant="h5">{title}</Typography>
              <Typography><span><b>Score: </b></span>{score}</Typography>
            </Link>
          </Grid>
          <Grid item xs={1} style={{textAlign: 'center', marginTop: 'auto', marginBottom: 'auto'}}>
            { favourited === false ? 
              <Button onClick={selectFavourite} style={{color: '#fab35c'}}>
                <FavoriteBorderIcon />
              </Button> : 
              <Button onClick={deselectFavourite} style={{color: '#fab35c'}}>
                <FavoriteIcon />
              </Button>
            }
          </Grid>
        </Grid>
      </Paper>
  );
}