import { Grid, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { useContext } from "react";
import { UserContext } from "../context/UserContext";

export default function NavBar() {
  const currentUser = useContext(UserContext);
  
  return (
    <Grid>
      <Grid style={{backgroundColor: '#fab35c', paddingTop: '20px', paddingBottom: '20px'}}>
        <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto'}}>
          <Grid item xs={6}>
            <Typography style={{color: '#FFFFFF'}}>Favourite Reddit Posts</Typography>
          </Grid>
          <Grid item xs={6}>
            <Grid container style={{flexDirection: 'row', justifyContent: 'right'}}>
              <Link to="/">
                <HomeIcon style={{color: '#FFFFFF', paddingRight: '20px'}} />
              </Link>
              {currentUser.user === "" ? null :
              <Grid>
                <Link to="/search">
                  <SearchIcon style={{color: '#FFFFFF', paddingRight: '20px'}} />
                </Link>
                <Link to="/favourites">
                  <FavoriteIcon style={{color: '#FFFFFF'}} />
                </Link>
              </Grid>
              }
            </Grid>
            
          </Grid>

        </Grid>
      </Grid>
      {currentUser.user === "" ? null : 
          <Grid style={{backgroundColor: '#FFFFFF'}}>
            <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto', justifyContent: 'right',}}>
              <Typography style={{color: '#000000'}}><b>Current User: </b>{currentUser.user}</Typography>
            </Grid>
          </Grid>
      }
    </Grid>
    
  );
}