import { Grid, Paper, Typography, Link, Button, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import React, {useState} from 'react';
import DeleteIcon from '@mui/icons-material/Delete';

export type favouriteArticleProps = {
  title: string,
  score: number,
  link: string,
  id: string,
  removeItem: (id: string) => void
}

export default function FavouriteArticle({title, score, link, id, removeItem}: favouriteArticleProps) {
  const [remove, setRemove] = useState(false);

  const removePopup = () => {
    if(remove === false) {
      setRemove(true);
    } else {
      setRemove(false);
    }
  }

  const handleClose = () => {
    setRemove(false);
  }

  const handleRemove = () => {
    setRemove(false);
    removeItem(id);
  }

  return (
    <Grid>
      <Paper style={{marginTop: '20px', marginBottom: '20px'}}>
        <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '20px', paddingBottom: '20px'}}>
          <Grid item xs={11}>
            <Link href={link} target="_blank" style={{color: '#000000', textDecoration: 'none'}}>
              <Typography variant="h5">{title}</Typography>
              <Typography><span><b>Score: </b></span>{score}</Typography>
            </Link>
          </Grid>
          <Grid item xs={1} style={{textAlign: 'center', marginTop: 'auto', marginBottom: 'auto'}}>
            <Button onClick={removePopup} style={{color: '#fab35c'}}>
              <DeleteIcon />
            </Button>
          </Grid>
        </Grid>
      </Paper>

      <Dialog open={remove} onClose={handleClose}>
        <DialogTitle style={{textAlign: 'center'}}>Remove Favourite</DialogTitle>
        <DialogContent style={{textAlign: 'center'}}>
          <Typography>Do you want to remove this favourite?</Typography>
        </DialogContent>
        <DialogActions>
          <Grid container style={{justifyContent: 'center'}}>
            <Button onClick={handleRemove} style={{backgroundColor: '#fab35c', color: '#FFFFFF', marginRight: '10px'}}>Yes</Button>
            <Button onClick={handleClose} style={{backgroundColor: '#e6e3e3', color: '#fab35c'}}>No</Button>
          </Grid>
        </DialogActions>
      </Dialog>
    </Grid>
  );
}