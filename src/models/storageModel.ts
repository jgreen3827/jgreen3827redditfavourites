export type userModel = {
  userId: number,
  userName: string,
  favourites: favouriteModel[]
}

export type favouriteModel = {
  id: string
}