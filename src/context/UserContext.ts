import React from "react";
export interface IUser {
  user: string,
  setUser: (user:string) => void
}
export const UserContext = React.createContext<IUser>({user: "", setUser: () => {return}});
