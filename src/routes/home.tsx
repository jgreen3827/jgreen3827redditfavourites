import { Button, Grid, MenuItem, Paper, Select, TextField, Typography } from "@mui/material";
import React, {useState, useEffect, useContext} from 'react';
import { userModel } from "../models/storageModel";
import { useNavigate } from 'react-router-dom';
import { UserContext } from "../context/UserContext";

export default function Home() {
  const [users, setUsers] = useState<userModel[] | null>(null);
  const [newUser, setNewUser] = useState("");
  const [userOptions, setUserOptions] = useState<any[]>([]);
  const [error, setError] = useState(false);
  const [errorText, setErrorText] = useState("");
  const currentUser = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    const inUsers = localStorage.getItem("users");
    if(inUsers !== null){
      const parsedUsers:userModel[] = JSON.parse(inUsers);
      setUsers(parsedUsers);
    }
  }, []);

  useEffect(() => {
    const tempOptions:any[] = [];
    if(users !== null) {
      users.forEach(element => {
        tempOptions.push(<MenuItem value={element.userName}>{element.userName}</MenuItem>)
      });
      setUserOptions(tempOptions);
    }
  }, [users]);

  const handleNewUserInput = (event: any) => {
    setNewUser(event.target.value);
  }

  const createNewUser = () => {
    //validation check
    if(newUser === ""){
      //cannot be empty
      setError(true);
      setErrorText("This field can not be empty");
      return;
    }
    if(users !== null){
      for(let i = 0; i < users.length; i++){
        if(newUser === users[i].userName){
          //name already exists
          setError(true);
          setErrorText("This user name already exists");
          return;
        }
      }
    }
    setError(false);
    
    let tempUsers: userModel[] = [];
    if(users !== null) {
      tempUsers = users;
      tempUsers.push({
        userId: tempUsers.length + 1,
        userName: newUser,
        favourites: []
      })
    } else {
      tempUsers.push({
        userId: 1,
        userName: newUser,
        favourites: []
      })
    }
    localStorage.setItem("users", JSON.stringify(tempUsers));
    setNewUser("");

    currentUser.setUser(newUser);
    navigate("/search");
  };

  const optionSelected = (event:any) => {
    currentUser.setUser(event.target.value);
    navigate("/search");
  }
  return (
    <Grid>
      <Grid container xs={7} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '30px', paddingBottom: '30px'}}>
        <Paper style={{width: '100%', padding: '50px'}}>
          <Grid style={{textAlign: 'center'}}>
            <Typography variant="h4">Profile</Typography>
          </Grid>
          <Grid style={{width: '80%', marginLeft: 'auto', marginRight: 'auto'}}>
            <Grid>
              <Typography>Enter New User</Typography>
              <TextField 
                id="outlined-basic" 
                variant="outlined" 
                style={{width: '100%', marginLeft: 'auto', marginRight: 'auto'}} 
                value={newUser}
                onChange={handleNewUserInput}
              />
              { error === false ? null :
                <Grid>
                  <Typography style={{color: '#FF0000'}}>{errorText}</Typography>
                </Grid> 
              }
              <Grid container style={{justifyContent: 'right', paddingTop: '10px'}}>
                <Button onClick={createNewUser} style={{backgroundColor: '#fab35c', color: '#FFFFFF'}}>Create New User</Button>
              </Grid>
            </Grid>
            <Grid>
              <Typography>Select Existing User</Typography>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={currentUser.user}
                onChange={optionSelected}
                style={{width: '100%', marginLeft: 'auto', marginRight: 'auto'}}
              >
                {userOptions}
              </Select>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}