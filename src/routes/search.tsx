import { Button, Grid, Paper, TextField, Typography } from "@mui/material";
import React, {useContext, useEffect, useState} from 'react';
import { useNavigate } from "react-router-dom";
import SubredditArticle from "../components/subredditArticle";
import { UserContext } from "../context/UserContext";

export default function Search() {
  const [userInput, setUserInput] = useState("");
  const [apiError, setApiError] = useState(false);
  const [articles, setArticles] = useState<any[]>([]);
  const currentUser = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    if(currentUser.user === ""){
      navigate("/");
    }
  });

  const inputChange  = (event:any) => {
    setUserInput(event.target.value);
  }

  const onSearch = () => {
    const alteredInput = userInput.replace(" ", "_")
    fetch(`https://www.reddit.com/r/${alteredInput}/hot.json?limit=9`).then(res=> {
      if(res.status !== 200) {
        setApiError(true);
        return;
      }
      setApiError(false);

      res.json().then(data => {
        if (data !== null){
          const tempArticles:any[] = [];
          data.data.children.forEach((element:any) => {
            tempArticles.push(<SubredditArticle title={element.data.title} score={element.data.score} link={element.data.url} id={element.data.id} />)
          });

          setArticles(tempArticles);
        }
      })
    })
  }

  return (
    <Grid>
      <Grid container xs={7} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '30px', }}>
        <Paper style={{width: '100%', padding: '20px'}}>
          <Grid style={{textAlign: 'center'}}>
            <Typography variant="h4">Search</Typography>
          </Grid>
          <Grid style={{width: '80%', marginLeft: 'auto', marginRight: 'auto'}}>
            <Grid>
              <Typography>Enter a subreddit</Typography>
              <TextField 
                id="outlined-basic" 
                variant="outlined" 
                style={{width: '100%', marginLeft: 'auto', marginRight: 'auto'}} 
                value={userInput}
                onChange={inputChange}
              />
              { apiError === true ?
                <Grid>
                  <Typography style={{color: '#FF0000'}}>ERROR: There was an error calling the API... Please Try Again</Typography>
                </Grid> :
                null
              } 
              <Grid container style={{justifyContent: 'right', paddingTop: '10px'}}>
                <Button onClick={onSearch} style={{backgroundColor: '#fab35c', color: '#FFFFFF'}}>Search</Button>
              </Grid>
            </Grid>
            <Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '30px', }}>
        <Paper style={{width: '100%', padding: '20px', backgroundColor: '#fab35c'}}>
          <Grid  xs={11.75} style={{marginLeft: 'auto', marginRight: 'auto'}}>
            <Typography variant='h4'>Search Results</Typography>
            {articles}
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}