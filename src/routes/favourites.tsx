import { Grid, Paper, Typography } from "@mui/material";
import React, {useContext, useEffect, useState} from 'react';
import { useNavigate } from "react-router-dom";
import FavouriteArticle from "../components/favouriteArticle";
import { UserContext } from "../context/UserContext";
import { favouriteModel, userModel } from "../models/storageModel";

export default function Favourites() {
  const [articles, setArticles] = useState<any[]>([]);
  const currentUser = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    const tempArticles:any[] = [];

    const removeItem = (id: string) => {
      const tempArticles:any[] = [];
      const fetchData = async (favourites: favouriteModel[]) => {
        for(let i = 0; i < favourites.length; i++) {
          const res = await fetch(`https://www.reddit.com/by_id/t3_${favourites[i].id}.json`) 
  
          const json = await res.json();
          json.data.children.forEach((element:any) => {
            tempArticles.push(<FavouriteArticle title={element.data.title}  score={element.data.score} link={element.data.url} id={element.data.id} removeItem={removeItem} />)
          });
        }
        setArticles(tempArticles);
      }
  
      const inUsers = localStorage.getItem("users");
      if(inUsers !== null){
        const parsedUsers:userModel[] = JSON.parse(inUsers);
        for(let i = 0; i < parsedUsers.length; i++) {
          if(parsedUsers[i].userName === currentUser.user){
           for(let j = 0; j < parsedUsers[i].favourites.length; j++){
             if(parsedUsers[i].favourites[j].id === id){
              parsedUsers[i].favourites.splice(j, 1);
              fetchData(parsedUsers[i].favourites);
             }
           }
          }
        }
        localStorage.setItem("users", JSON.stringify(parsedUsers));
      }
    }

    const fetchData = async (favourites: favouriteModel[]) => {
      for(let i = 0; i < favourites.length; i++) {
        const res = await fetch(`https://www.reddit.com/by_id/t3_${favourites[i].id}.json`) 
        
        const json = await res.json();
        json.data.children.forEach((element:any) => {
          tempArticles.push(<FavouriteArticle title={element.data.title}  score={element.data.score} link={element.data.url} id={element.data.id} removeItem={removeItem} />)
        });
      }
      setArticles(tempArticles);
    }

    if(currentUser.user === ""){
      navigate("/");
    } else {
      //get all favourites and display them
      const inUsers = localStorage.getItem("users");
      if(inUsers !== null){
        const parsedUsers:userModel[] = JSON.parse(inUsers);
        for(let i = 0; i < parsedUsers.length; i++) {
          if(parsedUsers[i].userName === currentUser.user){
            fetchData(parsedUsers[i].favourites);
            
          }
        }
      }
    }
  }, []);

  return (
    <Grid>
      <Grid container xs={11} style={{marginLeft: 'auto', marginRight: 'auto', paddingTop: '30px', }}>
        <Paper style={{width: '100%', padding: '20px', backgroundColor: '#fab35c'}}>
          <Grid  xs={11.75} style={{marginLeft: 'auto', marginRight: 'auto'}}>
            <Grid style={{textAlign: 'center'}}>
              <Typography variant='h4'>Favourites</Typography>
            </Grid>
            {articles.length === 0 ? 
              <Grid style={{textAlign: 'center', paddingTop: '20px'}}>
                <Typography>You don't currently have any favourites</Typography>
              </Grid> :
              <Grid>
                {articles}
              </Grid>
            }
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}